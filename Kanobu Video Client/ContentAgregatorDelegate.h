//
//  ContentAgregatorDelegate.h
//  Kanobu Video Client
//
//  Created by Denis on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ContentAgregatorDelegate <NSObject>
typedef enum {CANConnectionError,CANBodyParsed, CANVideoDownloadLinkAssumed}ContentAgregatorNotification;
-(void)contentAgregator:(id)sender notifyEvent:(ContentAgregatorNotification)notification withArgs:(id)args;
@end
