//
//  ContentAgregator.m
//  Kanobu Video Client
//
//  Created by Denis on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ContentAgregator.h"
#import "KanobuNetworkEngine.h"
@implementation VideoMetaInfo
@synthesize ID, pageLink, downloadLink, thumbnailImageLink, dateOfCreation, title;

// the only right creation method
+(VideoMetaInfo*)videoMetaInfoWithID:(int)ID andTitle:(NSString*)title andPageLink:(NSString*)pageLink andDownloadLink:(NSString*)downloadLink andThumbnailImageLink:(NSString*)thumbnailImageLink andDateOfCreation:(NSString*)dateOfCreation
{
    VideoMetaInfo *newInfo = [[VideoMetaInfo alloc] init];
    newInfo.ID = [NSNumber numberWithInt:ID];
    newInfo.title = title;
    newInfo.pageLink = pageLink;
    newInfo.downloadLink = downloadLink;
    newInfo.thumbnailImageLink = thumbnailImageLink;
    newInfo.dateOfCreation = dateOfCreation;
    return newInfo;
}

-(void)assumeDownloadLinkSynchronous
{
    self.downloadLink = [ContentAgregator getDownloadLinkForVideoMetaInfo:self];
}

@end


@implementation ContentAgregator
@synthesize videosMetaInfoList;
@synthesize delegate;
-(id)init
{
    self = [super init];
    network = nil;
    self.delegate = nil;
    self.videosMetaInfoList = [NSMutableArray new];
    // init network engine
    NSMutableDictionary *headerFields = [NSMutableDictionary dictionary];
    [headerFields setValue:@"iOS" forKey:@"x-client-identifier"];
    network = [[KanobuNetworkEngine alloc] initWithHostName:@"kanobu.ru" customHeaderFields:headerFields];
    [network useCache];
    
    
    _bodyDownloadOp = [network operationWithPath:@"video/new/"];
    [_bodyDownloadOp onCompletion:^(MKNetworkOperation *completedOperation)
     {
//         DLog(@"%@", [completedOperation responseString]);
         
		 // do your processing here
         [self parseBodyWithString:[completedOperation responseString]];
         
     }onError:^(NSError* error) {
         NSLog(@"### Error downloading html body of path 'video/new/'");
         //notify assigned delegate
         [self.delegate contentAgregator:self notifyEvent:CANConnectionError withArgs:error];
     }];
    
    [network enqueueOperation:_bodyDownloadOp];
    
    return self;
}

-(void)parseBodyWithString:(NSString *)bodyHTMLString
{
    NSError * error = nil;
//    parser = [[HTMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:@"http://kanobu.ru/video/new/"] error:&error];	
    parser = [[HTMLParser alloc] initWithString:bodyHTMLString error:&error];
	if (error) {
		NSLog(@"Error initing parser for body html: %@", error);
		parser = nil;
	}
	HTMLNode * bodyNode = [parser doc];
    HTMLNode * tableNode = [bodyNode findChildWithAttribute:@"class" matchingName:@"bigtable" allowPartial:YES];
    NSArray *vcardsOfVideos = [tableNode findChildrenWithAttribute:@"class" matchingName:@"vcard" allowPartial:YES];
    for (HTMLNode *videoVcard in vcardsOfVideos) {
        HTMLNode *photoNode = [videoVcard findChildWithAttribute:@"class" matchingName:@"photo" allowPartial:YES];
        HTMLNode *fnNode = [videoVcard findChildWithAttribute:@"class" matchingName:@"fn" allowPartial:YES];
        
        NSString *thumbnailLink = [photoNode getAttributeNamed:@"src"];
        NSString *videoLink = [NSString stringWithFormat:@"%@%@",@"http://kanobu.ru",[fnNode getAttributeNamed:@"href"]]; //get full link by adding href to resource name
        NSString *videoId = (NSString*)[[((NSString*)[[videoLink componentsSeparatedByString:@"/id"] objectAtIndex:1])componentsSeparatedByString:@"/"] objectAtIndex:0]; //parse link to get id
        NSString *videoTitle = [fnNode allContents];
        
        HTMLNode *adressNode = [videoVcard findChildOfClass:@"address"];
        HTMLNode *adressDateNode = [adressNode findChildTag:@"a"];
        NSString *dateAdded = [adressDateNode allContents];
        
        NSLog(@"TB:%@, TITLE:%@,VIDLINK:%@",thumbnailLink, videoTitle,videoLink);
        NSLog(@" TB LINK PATH: %@",[[NSURL URLWithString:thumbnailLink] path]);
        NSLog(@"ID:%@, DATE:%@",videoId,dateAdded);
        
        VideoMetaInfo *vmi = [VideoMetaInfo videoMetaInfoWithID:[videoId intValue] andTitle:videoTitle andPageLink:videoLink andDownloadLink:nil andThumbnailImageLink:thumbnailLink andDateOfCreation:dateAdded];
        [self.videosMetaInfoList addObject:vmi];
        
    }
    //notify assigned delegate
    [self.delegate contentAgregator:self notifyEvent:CANBodyParsed withArgs:nil];
}

- (void)initiateAsyncVideoPageDownloadAndParsingWithVideoMetaInfo:(VideoMetaInfo*)vmi
{
    NSLog(@"Initiated AsyncVideoPageDownloadAndParsingWithVideoMetaInfo.");
    NSURL *pageUrl = [NSURL URLWithString:vmi.pageLink];
    NSString *pagePath = pageUrl.path;
    NSLog(@"The path is %@",pagePath);
    NSString *correctedPath = [pagePath substringFromIndex:1];
    NSLog(@"Corrected path is %@",correctedPath);
    MKNetworkOperation *videoPageDownloadOp = [network operationWithPath:correctedPath];
    
    [videoPageDownloadOp onCompletion:^(MKNetworkOperation *completedOperation)
     {
         NSLog(@"Succsessfully completed download video page!");
//         DLog(@"%@", [completedOperation responseString]);
         vmi.downloadLink = [self parseDownloadLinkFromVideoPageContents:[completedOperation responseString]];
         //notify assigned delegate
         NSLog(@"Video meta info got its download link: %@", vmi.downloadLink);
         if (self.delegate == nil)
         {
             NSLog(@"### Warning content agregator delegate is nil!");
         }
         [self.delegate contentAgregator:self notifyEvent:CANVideoDownloadLinkAssumed withArgs:vmi];
     }onError:^(NSError* error) {
         NSLog(@"### Error downloading video page html body!");
         //notify assigned delegate
         if (self.delegate == nil)
         {
             NSLog(@"### Warning content agregator delegate is nil!");
         }
         [self.delegate contentAgregator:self notifyEvent:CANConnectionError withArgs:error];
     }];
    
    [network enqueueOperation:videoPageDownloadOp];
    NSLog(@"network enqued operation for AsyncVideoPageDownload with path:%@",correctedPath);
}

- (NSString *)applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}


-(MKNetworkEngine*)getNetworkEngine
{
    return (MKNetworkEngine*)network;
}

+(NSString*)getDownloadLinkForVideoMetaInfo:(VideoMetaInfo*)vmi
{
    NSError * error = nil;
	HTMLParser *myParser = [[HTMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:vmi.pageLink] error:&error];	
	if (error) {
		NSLog(@"Error: %@", error);
		myParser = nil;
		return nil;
	}
	HTMLNode * bodyNode = [myParser doc];
    HTMLNode * playerNode = [bodyNode findChildWithAttribute:@"class" matchingName:@"player player_centered" allowPartial:NO];
    NSString * downloadLink = [playerNode getAttributeNamed:@"href"];
    
    myParser = nil;
    NSLog(@"GOT DL LINK:%@",downloadLink);
    return downloadLink;
}

-(NSString*)parseDownloadLinkFromVideoPageContents:(NSString*)pageString
{
    NSError * error = nil;
	HTMLParser *myParser = [[HTMLParser alloc] initWithString:pageString error:&error];	
	if (error) {
		NSLog(@"Error: %@", error);
		myParser = nil;
		return nil;
	}
	HTMLNode * bodyNode = [myParser doc];
    HTMLNode * playerNode = [bodyNode findChildWithAttribute:@"class" matchingName:@"player player_centered" allowPartial:NO];
    NSString * downloadLink = [playerNode getAttributeNamed:@"href"];
    
    myParser = nil;
//    NSLog(@"Parsed link is:%@",downloadLink);
    return downloadLink;
}


@end
