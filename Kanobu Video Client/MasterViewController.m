//
//  MasterViewController.m
//  Kanobu Video Client
//
//  Created by Denis on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"

#import "ContentAgregator.h"

#import "AsyncImageView.h"

#import <SDWebImage/UIImageView+WebCache.h>

//constant tags are declared in header file

@interface MasterViewController () {
    NSMutableArray *_objects;
}
@end

@implementation MasterViewController

@synthesize detailViewController = _detailViewController, settingsButtonItem, indicator;

-(void)setContentAgregator:(ContentAgregator *)contentAgregator
{
    _contentAgregator = contentAgregator;
}
-(ContentAgregator*)contentAgregator
{
    return _contentAgregator;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Master", @"Master");
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            self.clearsSelectionOnViewWillAppear = NO;
            self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
        }
    }
//    self.contentAgregator = nil;
//    self.indicator = nil;
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

//    ** initial code **
//    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
//    self.navigationItem.rightBarButtonItem = addButton;
//    ** initial code **
    
    self.settingsButtonItem = [[UIBarButtonItem alloc] init];
    self.settingsButtonItem.title = @"Settings"; 
    self.navigationItem.leftBarButtonItem = self.settingsButtonItem;
    
    self.indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    self.indicator.frame = CGRectMake(round((self.view.frame.size.width - 25) / 2), round((self.view.frame.size.height - 25) / 2), 25, 25);
    
    [self.view addSubview:self.indicator];
    
    [self.indicator startAnimating];
    
    [self loadContent];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)loadContent
{
    self.contentAgregator = nil;
    self.contentAgregator = [[ContentAgregator alloc] init];
    self.contentAgregator.delegate = self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)insertAllObjectsFromContentAgregator;
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    ContentAgregator *ca = self.contentAgregator;
    for (VideoMetaInfo *vmi in ca.videosMetaInfoList)
    {
        [_objects addObject:vmi];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[_objects indexOfObject:vmi] inSection:0];
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    if (self.indicator.isAnimating)
    {
        [self.indicator stopAnimating];
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    ** initial code **
//    static NSString *CellIdentifier = @"Cell";
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        }
//    }
//
//
//    NSDate *object = [_objects objectAtIndex:indexPath.row];
//    cell.textLabel.text = [object description];
//    return cell;
//    ** initial code **
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    VideoMetaInfo *item = (VideoMetaInfo *)[_objects objectAtIndex:indexPath.row];
    cell.textLabel.text = item.title;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Added", @"Added"),item.dateOfCreation];
    [cell.imageView setImageWithURL:[NSURL URLWithString:item.thumbnailImageLink]
                   placeholderImage:[UIImage imageNamed:@"thumbnailPlaceholder"]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
    
    
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDate *object = [_objects objectAtIndex:indexPath.row];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
	    if (!self.detailViewController) {
	        self.detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPhone" bundle:nil];
            // additional necessary object passing like on init
            NSLog(@"Desc of self.contentAgregator to make sure its not nil when passing it to detailViewController %@",self.contentAgregator.description);
            // attention! the order of the next 2 lines is extremely important!
            // cuz setting the content agregator in detailViewController will automatically assign contentAgregator.delegate
            self.contentAgregator.delegate = nil;
            self.detailViewController.contentAgregator = self.contentAgregator;
            NSLog(@"Desc of detailViewController.contentAgregator %@",self.detailViewController.contentAgregator.description);
	    }
	    self.detailViewController.detailItem = object;
        [self.navigationController pushViewController:self.detailViewController animated:YES];
    } else {
        self.detailViewController.detailItem = object;
    }
    
 
}

#pragma mark -
#pragma mark <ContentAgregatorDelegate> methods

-(void)contentAgregator:(id)sender notifyEvent:(ContentAgregatorNotification)notification withArgs:(id)args
{
    UIAlertView *alert;
    switch (notification) {
        case CANConnectionError:
            
            alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", "Error alert view title") message:NSLocalizedString(@"Connection error! Retry...", @"Error alert message. Connection error.")  delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            alert.tag = TAG_ALERT_CONNECTION_ERROR;
            [alert show];
            
        case CANBodyParsed:
            [self insertAllObjectsFromContentAgregator];
            break;
            
        default:
            break;
    }
}

-(void)alertViewCancel:(UIAlertView *)alertView
{
    switch (alertView.tag) {
        case TAG_ALERT_CONNECTION_ERROR:
            [self loadContent];
            break;
            
        default:
            break;
    }
}
@end
