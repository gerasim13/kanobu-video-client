//
//  DetailViewController.h
//  Kanobu Video Client
//
//  Created by Denis on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ContentAgregatorDelegate.h"

@class ContentAgregator;

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate, ContentAgregatorDelegate>
{
    ContentAgregator *_contentAgregator;
    bool _asyncDownloadLinkRequestInProgress;
}
@property (strong, nonatomic) id detailItem;

@property (strong, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@property (nonatomic, retain) ContentAgregator *contentAgregator;
@property (nonatomic, retain) UIActivityIndicatorView *indicator;

-(IBAction)playVideo:(id)sender;

@end
