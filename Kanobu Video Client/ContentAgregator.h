//
//  ContentAgregator.h
//  Kanobu Video Client
//
//  Created by Denis on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTMLParser.h"
#import "ContentAgregatorDelegate.h"

@class KanobuNetworkEngine;

@interface VideoMetaInfo : NSObject
{
}
@property (nonatomic, retain) NSNumber *ID; // integer
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *pageLink;
@property (nonatomic, retain) NSString *downloadLink;
@property (nonatomic, retain) NSString *thumbnailImageLink;
@property (nonatomic, retain) NSString *dateOfCreation;
+(VideoMetaInfo*)videoMetaInfoWithID:(int)ID andTitle:(NSString*)title andPageLink:(NSString*)pageLink andDownloadLink:(NSString*)downloadLink andThumbnailImageLink:(NSString*)thumbnailImageLink andDateOfCreation:(NSString*)dateOfCreation;
-(void)assumeDownloadLinkSynchronous;

@end

@interface ContentAgregator : NSObject
{
    HTMLParser *parser;
    KanobuNetworkEngine *network;
    MKNetworkOperation *_bodyDownloadOp;
}
@property (nonatomic, retain) id <ContentAgregatorDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *videosMetaInfoList;
+(NSString*)getDownloadLinkForVideoMetaInfo:(VideoMetaInfo*)vmi;
-(MKNetworkEngine*)getNetworkEngine;
- (NSString *)applicationDocumentsDirectory;
-(void)parseBodyWithString:(NSString*)bodyHTMLString; //fills videosMetaInfoList
- (void)initiateAsyncVideoPageDownloadAndParsingWithVideoMetaInfo:(VideoMetaInfo*)vmi; // in order to assume video download link. This method would write the link into VMI and notify the delegate with VIM as args on success.
-(NSString*)parseDownloadLinkFromVideoPageContents:(NSString*)pageString;
@end
