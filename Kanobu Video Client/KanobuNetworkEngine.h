//
//  KanobuNetworkEngine.h
//  Kanobu Video Client
//
//  Created by Denis on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

@interface KanobuNetworkEngine : MKNetworkEngine
{
    
}
@property (nonatomic, retain) NSMutableDictionary* imagesForURLs;
-(NSString*)cacheDirectoryName; //override
@end
