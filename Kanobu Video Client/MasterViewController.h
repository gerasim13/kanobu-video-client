//
//  MasterViewController.h
//  Kanobu Video Client
//
//  Created by Denis on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContentAgregatorDelegate.h"

#define TAG_ALERT_CONNECTION_ERROR 3001

@class DetailViewController;
@class ContentAgregator;

@interface MasterViewController  : UITableViewController <ContentAgregatorDelegate, UIAlertViewDelegate>
{
    ContentAgregator *_contentAgregator;
}

@property (strong, nonatomic) DetailViewController *detailViewController;

@property (nonatomic, retain) UIBarButtonItem *settingsButtonItem;

@property (nonatomic, strong) ContentAgregator *contentAgregator;

@property (nonatomic, retain) UIActivityIndicatorView *indicator;


-(void)loadContent;
@end
