//
//  DetailViewController.m
//  Kanobu Video Client
//
//  Created by Denis on 6/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DetailViewController.h"
#import "ContentAgregator.h"
@interface DetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation DetailViewController

@synthesize detailItem = _detailItem;
@synthesize detailDescriptionLabel = _detailDescriptionLabel;
@synthesize masterPopoverController = _masterPopoverController;
@synthesize indicator;
#pragma mark - Content agregator interactions
-(void)setContentAgregator:(ContentAgregator *)incContentAgregator
{
    _contentAgregator = incContentAgregator;
    _contentAgregator.delegate = self;
}

-(ContentAgregator*)contentAgregator
{
    return _contentAgregator;
}

-(void)contentAgregator:(id)sender notifyEvent:(ContentAgregatorNotification)notification withArgs:(id)args
{
    switch (notification) {
        case CANVideoDownloadLinkAssumed:
            if (args!=nil)
            {
                VideoMetaInfo* vmi = (VideoMetaInfo*)args;
                if ([vmi isEqual:(VideoMetaInfo*)self.detailItem])
                {
                    // we've got right download link for this video;
                    [self configureView];
                }
            }
            break;
            
        default:
            break;
    }
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
        VideoMetaInfo *vmi = (VideoMetaInfo*)self.detailItem;
        if (vmi.downloadLink == nil)
        {
            if (!_asyncDownloadLinkRequestInProgress)
            {
                // need to assume download link cuz this video is under detail view for the first time
                [self.contentAgregator initiateAsyncVideoPageDownloadAndParsingWithVideoMetaInfo:vmi];
                _asyncDownloadLinkRequestInProgress = YES;
            }
        }
        else {
            // already got download link so do display
            self.detailDescriptionLabel.text = vmi.downloadLink;
            _asyncDownloadLinkRequestInProgress = NO;
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.detailDescriptionLabel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Detail", @"Detail");
        _asyncDownloadLinkRequestInProgress = NO;
    }
    return self;
}
							
#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

#pragma mark - Video

-(IBAction)playVideo:(id)sender  
{  
    NSString *filepath   =   [[NSBundle mainBundle] pathForResource:@"big-buck-bunny-clip" ofType:@"m4v"];  
    NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];  
    MPMoviePlayerController *moviePlayerController = [[MPMoviePlayerController alloc] initWithContentURL:fileURL];  
    [self.view addSubview:moviePlayerController.view];  
    moviePlayerController.fullscreen = YES;  
    [moviePlayerController play];  
} 

@end
