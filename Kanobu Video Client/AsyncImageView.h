//
//  AsyncImageView.h
//  Kanobu Video Client
//
//  Created by Denis on 6/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AsyncImageView : UIView
{
    UIImageView *mainImageView;
}
- (id)initWithFrame:(CGRect)frame andImagePlaceholder:(UIImage*)image;
- (void)loadImageFromURL:(NSURL*)downloadUrl byNetwork:(MKNetworkEngine*)network;
- (UIImage*) image;
@end
